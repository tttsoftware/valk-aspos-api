# Use the "Aspos REST API for webshops" of Valk Solutions in your PHP app

Documentation: https://webservicesdemo.aspos.nl/

## Usage examples

```
$connector = new \TTT\Aspos\ValkAspos('production', API_CUSTOMER, API_USERNAME, API_PASSWORD);

$product = $connector->getProductByScancode('2200010417753');
var_dump($product); // TTT\Aspos\Model\Product

$customerOrders = $connector->getCustomerOrders();
var_dump($customerOrders); // TTT\Aspos\Model\CustomerOrder[]

$webNodeRecords = $connector->getWebNodeRecords();
var_dump($webNodeRecords); // TTT\Aspos\Model\WebNodeRecord[]
```