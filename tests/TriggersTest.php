<?php

class TriggersTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_get_triggers()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $triggers = $connector->getTriggers();
        $this->assertIsArray($triggers);
        foreach ($triggers as $trigger) {
            $this->assertInstanceOf(TTT\Aspos\Model\Trigger::class, $trigger);
        }
    }

    /** @test */
    public function test_get_trigger()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $trigger = $connector->getTriggerById(1);
        $this->assertInstanceOf(TTT\Aspos\Model\Trigger::class, $trigger);

        $trigger = $connector->getTriggerById(0);
        $this->assertNull($trigger);
    }
}
