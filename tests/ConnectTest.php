<?php

class ConnectTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_client()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);
        $client = $connector->getClient();
        $this->assertInstanceOf('GuzzleHttp\Client', $client);
    }
}
