<?php

class StoresTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_get_stores()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $stores = $connector->getStores();
        $this->assertIsArray($stores);
        foreach ($stores as $store) {
            $this->assertInstanceOf(TTT\Aspos\Model\Store::class, $store);
        }
    }

    /** @test */
    public function test_get_store()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $store = $connector->getStoreById(39);
        $this->assertInstanceOf(TTT\Aspos\Model\Store::class, $store);

        $store = $connector->getStoreById(0);
        $this->assertNull($store);
    }
}
