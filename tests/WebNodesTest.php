<?php

class WebNodesTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_get_web_nodes()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $webNodes = $connector->getWebNodes();
        $this->assertIsArray($webNodes);
        foreach ($webNodes as $webNode) {
            $this->assertInstanceOf(TTT\Aspos\Model\WebNode::class, $webNode);
        }
    }

    /** @test */
    public function test_get_web_node_children()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $webNodes = $connector->getWebNodeChildrenByWebNodeId(1);
        $this->assertIsArray($webNodes);
        foreach ($webNodes as $webNode) {
            $this->assertInstanceOf(TTT\Aspos\Model\WebNode::class, $webNode);
        }
    }

    /** @test */
    public function test_get_web_node_products()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $webNodeProducts = $connector->getWebNodeProductsByWebNodeId(1);
        $this->assertIsArray($webNodeProducts);
        foreach ($webNodeProducts as $product) {
            $this->assertInstanceOf(TTT\Aspos\Model\Product::class, $product);
        }
    }

    /** @test */
    public function test_get_web_node_records()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $webNodeRecords = $connector->getWebNodeRecords();
        $this->assertIsArray($webNodeRecords);
        foreach ($webNodeRecords as $record) {
            $this->assertInstanceOf(TTT\Aspos\Model\WebNodeRecord::class, $record);
        }
    }

    /** @test */
    public function test_get_web_node_record()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $webNodeRecord = $connector->getWebNodeRecord(871020);
        if ($webNodeRecord) {
            $this->assertInstanceOf(\TTT\Aspos\Model\WebNodeRecord::class, $webNodeRecord);
        }
        else {
            $this->assertNull($webNodeRecord);
        }
    }
}
