<?php

class ProductsTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_scancode()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $scancode = '2200010417753';
        $response = $connector->getProductByScancode($scancode);
        $this->assertInstanceOf(TTT\Aspos\Model\Product::class, $response);

        $scancode = '5702016912547';
        $response = $connector->getProductByScancode($scancode);
        $this->assertNull($response);
    }

    /** @test */
    public function test_number()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $response = $connector->getProductByNumber('0001041775');
        $this->assertInstanceOf(TTT\Aspos\Model\Product::class, $response);

        $response = $connector->getProductByScancode('0');
        $this->assertNull($response);
    }

    /** @test */
    public function test_id()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $response = $connector->getProductById(41775);
        $this->assertInstanceOf(TTT\Aspos\Model\Product::class, $response);

        $response = $connector->getProductById(0);
        $this->assertNull($response);
    }

    /** @test */
    public function test_expand()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $query = new \TTT\Aspos\Query();
        $query->addExpand('brand');
        $query->addExpand('stockInfo');
        $response = $connector->getProductById(41775, $query);
        $this->assertInstanceOf(TTT\Aspos\Model\Product::class, $response);
        $this->assertInstanceOf(TTT\Aspos\Model\Brand::class, $response->brand);
    }

    /** @test */
    public function test_product_stock_info()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $response = $connector->getProductStockInfoByProductId(41775);
        $this->assertIsArray($response);
        foreach ($response as $model) {
            $this->assertInstanceOf(\TTT\Aspos\Model\StockInfo::class, $model);
        }

        $query = \TTT\Aspos\Query::fromArray(['storeId' => 39]);
        $response = $connector->getProductStockInfoByProductId(41775, $query);
        $this->assertIsArray($response);
        foreach ($response as $model) {
            $this->assertInstanceOf(\TTT\Aspos\Model\StockInfo::class, $model);
        }
    }

    /** @test */
    public function test_product_fields()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $response = $connector->getProductFields(41775);
        $this->assertIsArray($response);
        foreach ($response as $model) {
            $this->assertInstanceOf(\TTT\Aspos\Model\ProductField::class, $model);
        }
    }
}
