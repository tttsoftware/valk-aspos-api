<?php

class CustomerOrdersTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @return \TTT\Aspos\Model\CustomerOrder[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getCustomerOrders(): array
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $query = \TTT\Aspos\Query::fromArray(['limit' => 2]);

        return $connector->getCustomerOrders($query);
    }

    /** @test */
    public function test_get_customer_orders()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $customerOrders = $connector->getCustomerOrders();
        $customerOrderId = 0;
        $this->assertIsArray($customerOrders);
        foreach ($customerOrders as $customerOrder) {
            $customerOrderId = $customerOrder->id;
            $this->assertInstanceOf(TTT\Aspos\Model\CustomerOrder::class, $customerOrder);
        }

        $customerOrder = $connector->getCustomerOrderById($customerOrderId);
        $this->assertInstanceOf(TTT\Aspos\Model\CustomerOrder::class, $customerOrder);
    }

    /** @test */
    public function test_customer_orders_ordered()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $query = \TTT\Aspos\Query::fromArray(['limit' => 2]);
        $customerOrders = $connector->getCustomerOrders($query);

        $query = \TTT\Aspos\Query::fromArray(['limit' => 2, 'orderBy' => 'orderNumber']);
        $customerOrdersOrdered = $connector->getCustomerOrders($query);

        $this->assertCount(2, $customerOrders);
        $this->assertCount(2, $customerOrdersOrdered);
        $this->assertNotEquals($customerOrders, $customerOrdersOrdered);
    }

    /** @test */
    public function get_customer_orders_shipment_types()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $customerOrdersShipmentTypes = $connector->getCustomerOrdersShipmentTypes();
        $this->assertIsArray($customerOrdersShipmentTypes);
        foreach ($customerOrdersShipmentTypes as $customerOrdersShipmentType) {
            $this->assertInstanceOf(\TTT\Aspos\Model\ShipmentType::class, $customerOrdersShipmentType);
        }
    }

    /** @test */
    public function test_create_customer_order()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $orderLine = new \TTT\Aspos\Model\CustomerOrderLine();
        $orderLine->lineNr = 0;
        $orderLine->priceInclTax = 0.75;
        $orderLine->productId = 41775;
        $orderLine->quantity = 1;

        $customerOrder = new \TTT\Aspos\Model\CustomerOrder();
        $customerOrder->customerId = 1604;
        $customerOrder->isDeliveryOrder = false;
        $customerOrder->orderLines = [
            $orderLine,
        ];
        $customerOrder->orderType = 'PreOrderWebShop'; // default = PreOrderWebShop
        $customerOrder->customerReference = 'Test-3';
        $customerOrder->storeId = 39;
        $customerOrder->shipmentTypeId = 889; // 30 = Ophalen, 889 = Pakketdienst, 1158 = Verzenden

        try {
            $response = $connector->createCustomerOrder($customerOrder);
            $this->assertIsInt($response);
        }
        catch (\GuzzleHttp\Exception\ClientException $exception) {
            $this->assertEquals(400, $exception->getCode());
        }
    }

    /** @test */
    public function test_update_customer_order()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $newReference = null;
        $reference = null;

        foreach ($this->getCustomerOrders() as $customerOrder) {
            $newReference = ($customerOrder->customerReference ?? '') . 'T';

            $customerOrder->customerReference = $newReference;

            $connector->updateCustomerOrder($customerOrder);

            $customerOrder = $connector->getCustomerOrderById($customerOrder->id);
            $reference = $customerOrder->customerReference;

            break;
        }

        $this->assertNotNull($reference);
        $this->assertEquals($newReference, $reference);
    }

    /** @test */
    public function test_create_customer_order_payment()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $response = null;

        $customerOrders = $this->getCustomerOrders();
        foreach ($customerOrders as $customerOrder) {
            if ($customerOrder->orderType != 'PreOrderWebShop') {
                continue;
            }

            $customerOrderPayment = new \TTT\Aspos\Model\CustomerOrderPayment();
            $customerOrderPayment->customerOrderId = $customerOrder->id;
            $customerOrderPayment->amount = 0.75;
            $customerOrderPayment->paymentMethodId = 1;
            $response = $connector->createCustomerOrderPayment($customerOrderPayment);

            break;
        }

        $this->assertIsInt($response);
    }

    /** @test */
    public function test_get_customer_order_payments()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $customerOrders = $this->getCustomerOrders();
        foreach ($customerOrders as $customerOrder) {
            $customerOrderPayments = $connector->getCustomerOrderPaymentsByOrderId($customerOrder->id);
            $this->assertIsArray($customerOrderPayments);
            foreach ($customerOrderPayments as $customerOrderPayment) {
                $this->assertInstanceOf(TTT\Aspos\Model\CustomerOrderPayment::class, $customerOrderPayment);
            }

            break;
        }
    }

    /** @test */
    public function test_commit_customer_order()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $customerOrders = $this->getCustomerOrders();
        $this->assertArrayHasKey(0, $customerOrders);

        foreach ($customerOrders as $customerOrder) {
            $customerOrder = $connector->getCustomerOrderById($customerOrder->id);
            $response = $connector->commitCustomerOrder($customerOrder);
            $this->assertTrue($response);
            break;
        }
    }

    /** @test */
    public function test_convert_customer_order()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $customerOrders = $this->getCustomerOrders();
        $this->assertArrayHasKey(0, $customerOrders);

        foreach ($customerOrders as $customerOrder) {
            $response = $connector->convertCustomerOrderToDeliveryNoteById($customerOrder->id);
            $this->assertTrue($response);
            break;
        }
    }
}
