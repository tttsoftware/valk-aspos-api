<?php

class CustomersTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_get_customers()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $query = \TTT\Aspos\Query::fromArray(['limit' => 20]);
        $customers = $connector->getCustomers($query);
        $this->assertIsArray($customers);
        foreach ($customers as $customer) {
            $this->assertInstanceOf(TTT\Aspos\Model\Customer::class, $customer);
        }

        $customer = $connector->getCustomerById(1);
        $this->assertInstanceOf(TTT\Aspos\Model\Customer::class, $customer);

        $customer = $connector->getCustomerByCode('1');
        $this->assertInstanceOf(TTT\Aspos\Model\Customer::class, $customer);

        $customers = $connector->getCustomersByEmail('atze.teppema@tttsoftware.nl');
        $this->assertIsArray($customers);
        foreach ($customers as $customer) {
            $this->assertInstanceOf(TTT\Aspos\Model\Customer::class, $customer);
        }
    }

    /** @test */
    public function test_create_customer()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $customer = $connector->getCustomerByCode('1');
        $this->assertInstanceOf(TTT\Aspos\Model\Customer::class, $customer);

        $customer = new \TTT\Aspos\Model\Customer();
        $customer->code = '1';
        $customer->email = 'atze.teppema@tttsoftware.nl';
        $customer->firstName = 'Atze';
        $customer->lastName = 'Teppema';
        $customer->gender = 'Gender neutral';

        try {
            $response = $connector->createCustomer($customer);
            $this->assertIsInt($response);
        }
        catch (\GuzzleHttp\Exception\ClientException $exception) {
            $this->assertEquals(400, $exception->getCode());
        }
    }

    /** @test */
    public function test_update_customer()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $initials = str_split('abcdefghij');
        shuffle($initials);
        $initials = implode('', $initials);

        $query = new \TTT\Aspos\Query();
        $query->addField('id');
        $query->addField('initials');

        $customer = $connector->getCustomerByCode('1', $query);
        $this->assertInstanceOf(TTT\Aspos\Model\Customer::class, $customer);

        $customer->initials = $initials;
        $response = $connector->updateCustomer($customer);
        $this->assertTrue($response);

        $customer = $connector->getCustomerByCode('1', $query);
        $this->assertEquals($initials, $customer->initials);
    }
}
