<?php

class SyncTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function test_get_sync_products()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $token = '';
        $syncProducts = $connector->getSyncProducts($token);
        $this->assertIsArray($syncProducts);
        foreach ($syncProducts as $syncProduct) {
            $this->assertInstanceOf(TTT\Aspos\Model\SyncProduct::class, $syncProduct);
        }
    }

    /** @test */
    public function test_get_stock_infos()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $token = '';
        $syncStockInfos = $connector->getSyncStockInfos($token);
        $this->assertIsArray($syncStockInfos);
        foreach ($syncStockInfos as $syncStockInfo) {
            $this->assertInstanceOf(TTT\Aspos\Model\SyncProduct::class, $syncStockInfo);
        }
    }

    /** @test */
    public function test_get_web_nodes()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $token = '';
        $syncWebNodes = $connector->getSyncWebNodes($token);
        $this->assertIsArray($syncWebNodes);
        foreach ($syncWebNodes as $syncWebNode) {
            $this->assertInstanceOf(TTT\Aspos\Model\SyncWebNode::class, $syncWebNode);
        }
    }

    /** @test */
    public function test_get_web_node_records()
    {
        $connector = new \TTT\Aspos\ValkAspos(null, API_CUSTOMER, API_USERNAME, API_PASSWORD);

        $token = '';
        $syncWebNodeRecords = $connector->getSyncWebNodeRecords($token);
        $this->assertIsArray($syncWebNodeRecords);
        foreach ($syncWebNodeRecords as $syncWebNodeRecord) {
            $this->assertInstanceOf(TTT\Aspos\Model\SyncWebNodeRecord::class, $syncWebNodeRecord);
        }
    }
}
