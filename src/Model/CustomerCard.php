<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property-read ?int $id
 * @property ?int $blockedReasonCodeId
 * @property ?int $customerContactId
 * @property ?int $customerId
 * @property ?boolean $isBlocked
 * @property-read ?boolean $isEvl
 * @property string $number
 * @property-read string $registrationCode
 * @property-read ?float $totalBonusPoints
 * @property ?string $validUntilDate
 * @property-read \TTT\Aspos\Model\Customer $customer
 * @property-read \TTT\Aspos\Model\CustomerContact $customerContact
 */
class CustomerCard
{
    use Helpers\FromJson;
}
