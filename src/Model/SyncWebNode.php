<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?int $id
 * @property string $bigImageLink
 * @property string $code
 * @property string $description
 * @property string $displayType
 * @property string $documentName
 * @property ?boolean $isDeleted
 * @property string $memo
 * @property ?int $parentId
 * @property string $seoTitle
 * @property string $seoDescription
 * @property string $smallImageLink
 * @property ?int $sortOrder
 * @property string $status
 * @property string $type
 * @property \TTT\Aspos\Model\Translation[] $translations
 * @property string $creationDate
 * @property string $lastModifiedDate
 */
class SyncWebNode
{
    use Helpers\FromJson;
}
