<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?boolean $bonusPointsAllowed
 * @property ?boolean $discountAllowed
 * @property ?boolean $visibleInWebshop
 * @property ?boolean $visibleInLists
 * @property ?float $priceFactor
 * @property ?float $priceRounding
 * @property ?int $preferredSupplierId
 * @property ?int $storeId
 * @property-read ?int $brandId
 * @property \TTT\Aspos\Model\BrandStoreSettingsAdditionalParameters $additionalParameters
 */
class BrandStoreSettings
{
    use Helpers\FromJson;
}
