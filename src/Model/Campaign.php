<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property-read ?int $id
 * @property string $type
 * @property string $description
 * @property ?boolean $isActive
 * @property ?boolean $isVoucherCampaign
 * @property ?boolean $optInRequired
 * @property string $voucherType
 * @property string $voucherAmount
 * @property ?string $startDate
 * @property ?string $endDate
 * @property string $externalCode
 * @property ?int $daysBefore
 * @property ?int $daysAfter
 * @property string $customerEventType
 * @property ?int $createdByGroupUserId
 * @property ?int $modifiedByGroupUserId
 */
class Campaign
{
    use Helpers\FromJson;
}
