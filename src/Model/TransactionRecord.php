<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?float $amountExclTax
 * @property ?float $amountInclTax
 * @property-read ?int $bonusPoints
 * @property-read ?int $customerOrderLineId
 * @property string $description
 * @property ?float $discountAmount
 * @property ?int $discountId
 * @property-read int $id
 * @property ?float $listPrice
 * @property string $notes
 * @property-read float $orderQuantity
 * @property ?float $priceExclTax
 * @property ?float $priceInclTax
 * @property-read \TTT\Aspos\Model\Product $product
 * @property ?int $productId
 * @property float $productQuantity
 * @property ?float $purchasePrice
 * @property string $scanCode
 * @property string $serialNumber
 * @property-read ?float $taxAmount
 * @property-read ?int $taxCodeId
 * @property ?float $unitPrice
 * @property ?int $warehouseId
 * @property ?int $warehouseLocationId
 * @property ?int $productKitId
 * @property ?int $parentProductId
 * @property ?int $kitGroupId
 * @property \TTT\Aspos\Model\TransactionRecordExtension[] $extensions
 */
class TransactionRecord
{
    use Helpers\FromJson;
}
