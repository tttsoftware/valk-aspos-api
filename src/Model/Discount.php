<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?int $id
 * @property string $description
 * @property string $ticketDescription
 * @property ?string $startDate
 * @property ?string $startTime
 * @property ?string $endDate
 * @property ?string $endTime
 * @property string $discountCategory
 * @property string $discountKind
 * @property string $flyerCode
 * @property-read string $flyerDescription
 * @property string $webshopLabelTypeCode
 * @property-read string $webshopLabelTypeDescription
 * @property ?boolean $isActive
 * @property ?boolean $hideOnWebshopPage
 * @property ?\TTT\Aspos\Model\DayOfWeekFlagged $weekDays
 * @property ?int $discountCategoryRelatedId
 * @property ?int $priceListId
 * @property ?float $minimumQuantity
 * @property ?float $maximumQuantity
 * @property ?float $totalAmount
 * @property \TTT\Aspos\Model\Image[] $images
 * @property \TTT\Aspos\Model\PriceList $priceList
 * @property \TTT\Aspos\Model\Product $product
 * @property ?float $discountCheapest
 * @property ?int $discountCheapestQuantity
 */
class Discount
{
    use Helpers\FromJson;
}
