<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?int $storeId
 * @property string $email
 */
class HasPasswordRequest
{
    use Helpers\FromJson;
}
