<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property-read ?int $id
 * @property string $value
 * @property string $languageCode
 */
class MasterTableItemValue
{
    use Helpers\FromJson;
}
