<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property-read int $id
 * @property string $name
 * @property ?int $customerId
 * @property-read \TTT\Aspos\Model\Customer $customer
 * @property ?int $customerContactId
 * @property-read \TTT\Aspos\Model\CustomerContact $customerContact
 * @property ?int $storeId
 * @property string $typeCode
 * @property-read string $typeDescription
 * @property string $statusCode
 * @property ?string $startDate
 * @property ?string $endDate
 * @property string $notes
 * @property string $contact
 * @property string $phoneNumber
 * @property string $emailAddress
 * @property string $address1
 * @property string $address2
 * @property string $title
 * @property \TTT\Aspos\Model\ProjectRecord[] $projectRecords
 */
class Project
{
    use Helpers\FromJson;
}
