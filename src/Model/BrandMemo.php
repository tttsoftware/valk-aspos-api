<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?int $id
 * @property ?int $brandId
 * @property string $memoType
 * @property string $text
 * @property ?int $storeId
 * @property \TTT\Aspos\Model\Brand $brand
 */
class BrandMemo
{
    use Helpers\FromJson;
}
