<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property float $oldBalance
 * @property float $newBalance
 * @property string $brandName
 * @property \TTT\Aspos\Model\GiftCard $card
 * @property int $transactionId
 * @property string $providerTransactionId
 * @property int $providerResultCode
 * @property string $providerResultDescription
 */
class ReloadedGiftCard
{
    use Helpers\FromJson;
}
