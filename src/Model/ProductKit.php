<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?int $parentProductId
 * @property \TTT\Aspos\Model\Product $parentProduct
 * @property \TTT\Aspos\Model\ProductKitChild[] $children
 */
class ProductKit
{
    use Helpers\FromJson;
}
