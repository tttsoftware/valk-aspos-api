<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property string $name
 * @property \TTT\Aspos\Model\SortOrder $sortOrder
 */
class SearchSortField
{
    use Helpers\FromJson;
}
