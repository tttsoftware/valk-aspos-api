<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property ?int $id
 * @property string $code
 * @property string $name
 * @property string $street
 * @property string $houseNumber
 * @property string $houseNumberExtension
 * @property string $postalCode
 * @property string $city
 * @property string $countryCode
 * @property string $phoneNumber
 * @property string $email
 * @property string $faxNumber
 * @property string $taxNumber
 * @property string $coCNumber
 * @property string $bankAccount
 * @property ?string $openingDate
 * @property ?string $closingDate
 * @property string $memo
 * @property int $branchNumber
 * @property string $formulaCode
 * @property-read string $formulaDescription
 * @property string $status
 * @property string $website
 * @property string $languageCode
 * @property \TTT\Aspos\Model\StoreOpeningTimes $openingTimes
 * @property \TTT\Aspos\Model\Image[] $images
 * @property \TTT\Aspos\Model\StoreLocation $location
 * @property \TTT\Aspos\Model\StoreWebSettings $webSettings
 * @property \TTT\Aspos\Model\StoreExtendedProperty[] $extendedProperties
 * @property \TTT\Aspos\Model\Warehouse[] $warehouses
 * @property \TTT\Aspos\Model\Store[] $children
 * @property ?int $regionManagerId
 * @property \TTT\Aspos\Model\GroupUser $regionManager
 * @property ?int $branchManagerId
 * @property \TTT\Aspos\Model\GroupUser $branchManager
 * @property string $type
 * @property string $locationType
 */
class Store
{
    use Helpers\FromJson;
}
