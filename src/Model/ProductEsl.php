<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property-read ?int $id
 * @property string $code
 * @property ?int $productId
 * @property ?int $storeId
 * @property string $state
 * @property string $processState
 * @property string $layoutCode
 * @property-read string $layoutDescription
 * @property string $typeCode
 * @property-read string $typeDescription
 * @property string $xmlSettings
 * @property-read \TTT\Aspos\Model\Product $product
 */
class ProductEsl
{
    use Helpers\FromJson;
}
