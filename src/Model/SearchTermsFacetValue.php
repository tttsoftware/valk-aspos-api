<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property int $count
 * @property boolean $isSelected
 * @property string $value
 */
class SearchTermsFacetValue
{
    use Helpers\FromJson;
}
