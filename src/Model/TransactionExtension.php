<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $value
 */
class TransactionExtension
{
    use Helpers\FromJson;
}
