<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @method static self Android()
 * @method static self Apple()
 * @method static self Microsoft()
 */
class DevicePlatform extends \Spatie\Enum\Enum
{
    use Helpers\FromJson;
}
