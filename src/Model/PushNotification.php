<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property string $templateCode
 * @property object $templateParameters
 * @property ?boolean $sendToApple
 * @property ?boolean $sendToAndroid
 * @property array $tags
 */
class PushNotification
{
    use Helpers\FromJson;
}
