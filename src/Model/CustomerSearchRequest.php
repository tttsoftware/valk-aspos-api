<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property string $searchText
 * @property ?int $storeId
 * @property string $fields
 * @property string $expand
 * @property string $locale
 * @property int $offset
 * @property ?int $limit
 */
class CustomerSearchRequest
{
    use Helpers\FromJson;
}
