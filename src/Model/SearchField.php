<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property string $name
 * @property ?float $boost
 * @property ?boolean $usePrefixMatching
 * @property ?boolean $useSuffixMatching
 */
class SearchField
{
    use Helpers\FromJson;
}
