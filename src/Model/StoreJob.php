<?php

// this file is auto-generated by ttt/openapi-model-creator
// don't edit this file manually

namespace TTT\Aspos\Model;

/**
 * @property-read ?int $id
 * @property-read ?string $createdDate
 * @property-read ?string $lastModifiedDate
 * @property ?boolean $isSingleJobSession
 * @property ?int $createdByStoreId
 * @property ?int $createdByGroupUserId
 * @property ?int $storeId
 * @property ?int $warehouseId
 * @property string $type
 * @property string $sortOrderType
 * @property string $sortOrder
 * @property string $jobDocumentId
 * @property string $documentId
 * @property string $jobStatus
 * @property ?string $finishedDate
 * @property ?int $processByGroupUserId
 * @property ?boolean $isProcessed
 * @property ?string $processedDate
 * @property ?int $relatedId
 * @property ?boolean $isParked
 * @property ?int $lastModifiedByGroupUserId
 * @property string $xmlFields
 * @property \TTT\Aspos\Model\StoreJobLine[] $jobLines
 * @property string $remarks
 */
class StoreJob
{
    use Helpers\FromJson;
}
