<?php

namespace TTT\Aspos\Domain;

use GuzzleHttp\Exception\ClientException;
use TTT\Aspos\Model\Trigger;
use TTT\Aspos\Query;

trait Triggers
{
    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Trigger[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTriggers(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path('/api/triggers'));

        $body = $response->getBody()->getContents();

        return Trigger::fromJson($body);
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Trigger|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTriggerById(int $id, ?Query $query = null): ?Trigger
    {
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($query->path("/api/triggers/$id"));

            $body = $response->getBody()->getContents();

            $stores = Trigger::fromJson($body);

            if (count($stores)) {
                return $stores[0];
            }

            return null;
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }

    /**
     * @param Trigger $trigger
     * @return int|false
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createTrigger(Trigger $trigger)
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->post($query->path('/api/triggers'), $query->postOptions($trigger));

        if ($response->getStatusCode() == 201) {
            return (int) $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * @param Trigger $trigger
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateTrigger(Trigger $trigger): bool
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->patch("/api/triggers/$trigger->id", $query->postOptions($trigger));

        if ($response->getStatusCode() == 200) {
            return true;
        }

        return false;
    }
}
