<?php

namespace TTT\Aspos\Domain;

use GuzzleHttp\Exception\ClientException;
use TTT\Aspos\Model\Store;
use TTT\Aspos\Query;

trait Stores
{
    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Store[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStores(?Query $query = null): array
    {
        $query = $query ?? new Query();
        $query->addQuery('onlyCurrentLevelAndChildren', 'true');

        $response = $this->getClient()->get($query->path('/api/stores'));

        $body = $response->getBody()->getContents();

        return Store::fromJson($body);
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Store|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStoreById(int $id, ?Query $query = null): ?Store
    {
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($query->path("/api/stores/$id"));

            $body = $response->getBody()->getContents();

            $stores = Store::fromJson($body);

            if (count($stores)) {
                return $stores[0];
            }

            return null;
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }
}
