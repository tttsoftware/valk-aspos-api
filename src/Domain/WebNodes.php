<?php

namespace TTT\Aspos\Domain;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use TTT\Aspos\Model\Product;
use TTT\Aspos\Model\WebNode;
use TTT\Aspos\Model\WebNodeRecord;
use TTT\Aspos\Query;

trait WebNodes
{
    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\WebNode[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWebNodes(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path('/api/web-nodes'));

        $body = $response->getBody()->getContents();

        return WebNode::fromJson($body);
    }

    /**
     * todo; catch 404?
     *
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\WebNode[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWebNodeChildrenByWebNodeId(int $id, ?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/web-nodes/$id/children"));

        $body = $response->getBody()->getContents();

        return WebNode::fromJson($body);
    }

    /**
     * todo; catch 404?
     *
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Product[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWebNodeProductsByWebNodeId(int $id, ?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/web-nodes/$id/products"));

        $body = $response->getBody()->getContents();

        return Product::fromJson($body);
    }

    /**
     * todo; catch 404?
     *
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\WebNodeRecord[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWebNodeRecords(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/web-node-records"));

        $body = $response->getBody()->getContents();

        return WebNodeRecord::fromJson($body);
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\WebNodeRecord|null
     * @throws GuzzleException
     */
    public function getWebNodeRecord(int $id, ?Query $query = null): ?WebNodeRecord
    {
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($query->path("/api/web-node-records/$id"));

            $body = $response->getBody()->getContents();

            return WebNodeRecord::fromJson($body);
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }
}
