<?php

namespace TTT\Aspos\Domain;

use GuzzleHttp\Exception\ClientException;
use TTT\Aspos\Model\Customer;
use TTT\Aspos\Query;

trait Customers
{
    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Customer[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomers(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path('/api/customers'));

        $body = $response->getBody()->getContents();

        return Customer::fromJson($body);
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Customer|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomerById(int $id, ?Query $query = null): ?Customer
    {
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($query->path("/api/customers/$id"));

            $body = $response->getBody()->getContents();

            return Customer::fromJson($body);
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }

    /**
     * @param string $code
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Customer|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomerByCode(string $code, ?Query $query = null): ?Customer
    {
        $code = urlencode($code);
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($query->path("/api/customers/code/$code"));

            $body = $response->getBody()->getContents();

            return Customer::fromJson($body);
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }

    /**
     * @param string $email
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Customer[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomersByEmail(string $email, ?Query $query = null): array
    {
        $email = urlencode($email);
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/customers/email/$email"));

        $body = $response->getBody()->getContents();

        return Customer::fromJson($body);
    }

    /**
     * @param \TTT\Aspos\Model\Customer $customer
     * @return int|false
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createCustomer(Customer $customer)
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->post($query->path('/api/customers'), $query->postOptions($customer));

        if ($response->getStatusCode() == 201) {
            return (int) $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * @param \TTT\Aspos\Model\Customer $customer
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateCustomer(Customer $customer): bool
    {
        $query = $query ?? new Query();

        if (isset($customer->code)) {
            $customerCode = $customer->code;
            $customer->code = null;
        }

        $response = $this->getClient()->patch("/api/customers/$customer->id", $query->postOptions($customer));

        if (isset($customer->code)) {
            $customer->code = $customerCode;
        }

        if ($response->getStatusCode() == 200) {
            return true;
        }

        return false;
    }
}
