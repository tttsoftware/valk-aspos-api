<?php

namespace TTT\Aspos\Domain;

use TTT\Aspos\Model\PaymentMethod;
use TTT\Aspos\Query;

trait PaymentMethods
{
    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\PaymentMethod[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPaymentMethods(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path('/api/payment-methods'));

        $body = $response->getBody()->getContents();

        return PaymentMethod::fromJson($body);
    }
}
