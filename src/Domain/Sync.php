<?php

namespace TTT\Aspos\Domain;

use TTT\Aspos\Model\SyncProduct;
use TTT\Aspos\Model\SyncWebNode;
use TTT\Aspos\Model\SyncWebNodeRecord;
use TTT\Aspos\Query;

trait Sync
{
    public string $newSyncProductsToken = '';
    public string $newSyncStockInfosToken = '';
    public string $newSyncWebNodesToken = '';
    public string $newSyncWebNodeRecordsToken = '';

    /**
     * @param string $token
     * @param ?int $storeId
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\SyncProduct[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSyncProducts(string $token, ?int $storeId = null, ?Query $query = null): array
    {
        $query = $query ?? new Query();
        $query->addQuery('token', $token);
        if ($storeId) {
            $query->addQuery('storeId', $storeId);
        }

        $response = $this->getClient()->get($query->path('/api/sync/web-products'));

        $this->newSyncProductsToken = $response->getHeaderLine('x-Sync-Token');

        $body = $response->getBody()->getContents();

        return SyncProduct::fromJson($body);
    }

    /**
     * @param string $token
     * @param ?int $storeId
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\SyncStockInfo[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSyncStockInfos(string $token, ?int $storeId = null, ?Query $query = null): array
    {
        $query = $query ?? new Query();
        $query->addQuery('token', $token);
        if ($storeId) {
            $query->addQuery('storeId', $storeId);
        }

        $response = $this->getClient()->get($query->path('/api/sync/web-products-stock-info'));

        $this->newSyncStockInfosToken = $response->getHeaderLine('x-Sync-Token');

        $body = $response->getBody()->getContents();

        return SyncProduct::fromJson($body);
    }

    /**
     * @param string $token
     * @param int|null $storeId
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\SyncWebNode[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSyncWebNodes(string $token, ?int $storeId = null, ?Query $query = null): array
    {
        $query = $query ?? new Query();
        $query->addQuery('token', $token);
        if ($storeId) {
            $query->addQuery('storeId', $storeId);
        }

        $response = $this->getClient()->get($query->path('/api/sync/web-nodes'));

        $this->newSyncWebNodesToken = $response->getHeaderLine('x-Sync-Token');

        $body = $response->getBody()->getContents();

        return SyncWebNode::fromJson($body);
    }

    /**
     * @param string $token
     * @param int|null $storeId
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\SyncWebNodeRecord[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSyncWebNodeRecords(string $token, ?int $storeId = null, ?Query $query = null): array
    {
        $query = $query ?? new Query();
        $query->addQuery('token', $token);
        if ($storeId) {
            $query->addQuery('storeId', $storeId);
        }

        $response = $this->getClient()->get($query->path('/api/sync/web-node-records'));

        $this->newSyncWebNodeRecordsToken = $response->getHeaderLine('x-Sync-Token');

        $body = $response->getBody()->getContents();

        return SyncWebNodeRecord::fromJson($body);
    }
}
