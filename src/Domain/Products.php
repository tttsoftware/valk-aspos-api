<?php

namespace TTT\Aspos\Domain;

use GuzzleHttp\Exception\ClientException;
use TTT\Aspos\Model\Product;
use TTT\Aspos\Model\ProductField;
use TTT\Aspos\Model\StockInfo;
use TTT\Aspos\Query;

trait Products
{
    /**
     * @param string $scancode
     * @param \TTT\Aspos\Query|null $query
     * @return ?\TTT\Aspos\Model\Product
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProductByScancode(string $scancode, ?Query $query = null): ?Product
    {
        $scancode = urlencode($scancode);
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($this->baseUrl . $query->path("/api/products/scancode/$scancode"));

            $body = $response->getBody()->getContents();

            return Product::fromJson($body);
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }

    /**
     * @param string $number
     * @param \TTT\Aspos\Query|null $query
     * @return ?\TTT\Aspos\Model\Product
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProductByNumber(string $number, ?Query $query = null): ?Product
    {
        $number = urlencode($number);
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($this->baseUrl . $query->path("/api/products/number/$number"));

            $body = $response->getBody()->getContents();

            return Product::fromJson($body);
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\Product|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProductById(int $id, ?Query $query = null): ?Product
    {
        $query = $query ?? new Query();

        try {
            $response = $this->getClient()->get($query->path("/api/products/$id"));

            $body = $response->getBody()->getContents();

            return Product::fromJson($body);
        }
        catch (ClientException $exception) {
            if ($exception->getCode() == 404) {
                return null;
            }

            throw $exception;
        }
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\StockInfo[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProductStockInfoByProductId(int $id, ?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/products/$id/stock-info"));

        $body = $response->getBody()->getContents();

        return StockInfo::fromJson($body);
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\ProductField[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProductFields(int $id, ?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/products/$id/fields"));

        $body = $response->getBody()->getContents();

        return ProductField::fromJson($body);
    }
}
