<?php

namespace TTT\Aspos\Domain;

use TTT\Aspos\Model\CustomerOrder;
use TTT\Aspos\Model\CustomerOrderCommitRequest;
use TTT\Aspos\Model\CustomerOrderPayment;
use TTT\Aspos\Model\ShipmentType;
use TTT\Aspos\Query;

trait CustomerOrders
{
    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\CustomerOrder[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomerOrders(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path('/api/customer-orders'));

        $body = $response->getBody()->getContents();

        return CustomerOrder::fromJson($body);
    }

    /**
     * @param int $id
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\CustomerOrder|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomerOrderById(int $id, ?Query $query = null): ?CustomerOrder
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path("/api/customer-orders/$id"));

        $body = $response->getBody()->getContents();

        return CustomerOrder::fromJson($body);
    }

    /**
     * @param \TTT\Aspos\Query|null $query
     * @return \TTT\Aspos\Model\ShipmentType[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomerOrdersShipmentTypes(?Query $query = null): array
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->get($query->path('/api/customer-orders/shipment-types'));

        $body = $response->getBody()->getContents();

        return ShipmentType::fromJson($body);
    }

    /**
     * @param \TTT\Aspos\Model\CustomerOrder $customerOrder
     * @return int|false
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createCustomerOrder(CustomerOrder $customerOrder)
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->post('/api/customer-orders', $query->postOptions($customerOrder));

        if ($response->getStatusCode() == 201) {
            return (int) $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * @param \TTT\Aspos\Model\CustomerOrder $customerOrder
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateCustomerOrder(CustomerOrder $customerOrder): bool
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->patch("/api/customer-orders/$customerOrder->id", $query->postOptions($customerOrder));

        if ($response->getStatusCode() == 200) {
            return true;
        }

        return false;
    }

    /**
     * @param int $orderId
     * @param \TTT\Aspos\Query|null $query
     * @return CustomerOrderPayment[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCustomerOrderPaymentsByOrderId(int $orderId, ?Query $query = null): array
    {
        $query = $query ?? new Query();
        $query->addQuery('orderId', $orderId);

        $response = $this->getClient()->get($query->path('/api/customer-order-payments'));

        $body = $response->getBody()->getContents();

        return CustomerOrderPayment::fromJson($body);
    }

    /**
     * @param \TTT\Aspos\Model\CustomerOrderPayment $customerOrderPayment
     * @return int|false
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createCustomerOrderPayment(CustomerOrderPayment $customerOrderPayment)
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->post($query->path('/api/customer-order-payments'), $query->postOptions($customerOrderPayment));

        if ($response->getStatusCode() == 201) {
            return (int) $response->getBody()->getContents();
        }

        return false;
    }

    /**
     * @param \TTT\Aspos\Model\CustomerOrder $customerOrder
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function commitCustomerOrder(CustomerOrder $customerOrder): bool
    {
        return $this->commitCustomerOrderById($customerOrder->id);
    }

    /**
     * Commit a specific pre-order.
     *
     * After a successful commit the product stock wil be processed
     *
     * @param int $id
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     * todo: throw Error object if validation errors occur
     */
    public function commitCustomerOrderById(int $id): bool
    {
        $query = $query ?? new Query();

        $customerOrderCommitRequest = new CustomerOrderCommitRequest();
        $customerOrderCommitRequest->skipBonusPointMutations = false;
        $customerOrderCommitRequest->exportOrder = false;
        $customerOrderCommitRequest->asDirectSales = false;

        $response = $this->getClient()->post(
            $query->path("/api/customer-orders/$id/Commit"),
            $query->postOptions($customerOrderCommitRequest),
        );

        if ($response->getStatusCode() == 200) {
            return true;
        }

        return false;
    }

    /**
     * @param CustomerOrder $customerOrder
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function convertCustomerOrderToDeliveryNote(CustomerOrder $customerOrder): bool
    {
        return $this->convertCustomerOrderToDeliveryNoteById($customerOrder->id);
    }

    /**
     * @param int $id
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     * todo: throw Error object if validation errors occur
     */
    public function convertCustomerOrderToDeliveryNoteById(int $id): bool
    {
        $query = $query ?? new Query();

        $response = $this->getClient()->post(
            $query->path("/api/customer-orders/$id/ConvertToDeliveryNote"),
        );

        if ($response->getStatusCode() == 200) {
            return true;
        }

        return false;
    }
}
