<?php

namespace TTT\Aspos;

class Query
{
    public ?int $offset = null;
    public ?int $limit = null;
    public ?string $orderBy = null; // todo Asc,Desc ordering
    public ?int $storeId = null;
    public ?string $locale = null;

    private array $queries = []; // extra query parts in path
    private array $expands = []; // expand result
    private array $fields = []; // get specific fields

    public static function fromArray($array): self
    {
        $query = new static();

        foreach ($query->getClassFields() as $field) {
            $query->$field = $array[$field] ?? null;
        }

        return $query;
    }

    public function addQuery($key, $value)
    {
        $this->queries[$key] = $value;
    }

    public function addExpand($key)
    {
        $this->expands[$key] = $key;
    }

    public function addField($key)
    {
        $this->fields[$key] = $key;
    }

    public function path($apiPath): string
    {
        $path = $apiPath;

        $data = [];
        foreach ($this->getClassFields() as $field) {
            if (!is_null($this->$field)) {
                $data[$field] = $this->$field;
            }
        }
        foreach ($this->queries as $key => $value) {
            $data[$key] = $value;
        }
        if ($this->expands) {
            $data['expand'] = implode(',', $this->expands);
        }
        if ($this->fields) {
            $data['fields'] = implode(',', $this->fields);
        }
        if ($this->orderBy) {
            $data['OrderBy'] = $this->orderBy;
        }

        if ($data) {
            $q = http_build_query($data);

            if (strpos($path, '?') === false) {
                $path .= "?$q";
            } else {
                $path .= "&$q";
            }
        }

        return $path;
    }

    public function postOptions($body = null): array
    {
        $options = [
            'headers' => [
                'content-type' => 'application/json',
            ],
        ];

        if ($body !== null) {
            if (!is_string($body)) {
                $json = json_encode($body);
            } else {
                $json = $body;
            }
            $options['body'] = $json;
        }

        return $options;
    }

    /**
     * @return string[]
     */
    private function getClassFields(): array
    {
        $fields = [];

        $reflection = new \ReflectionClass(static::class);
        foreach ($reflection->getProperties() as $property) {
            if ($property->isPrivate()) {
                continue;
            }

            $fields[] = $property->getName();
        }

        return $fields;
    }
}
