<?php

namespace TTT\Aspos;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\OAuth2Middleware;
use kamermans\OAuth2\Persistence\FileTokenPersistence;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ValkAspos
{
    use Domain\Customers;
    use Domain\CustomerOrders;
    use Domain\PaymentMethods;
    use Domain\Products;
    use Domain\Stores;
    use Domain\Sync;
    use Domain\Triggers;
    use Domain\WebNodes;

    private string $baseUrl;
    private string $username;
    private string $password;

    private string $tokenPath;
    private HandlerStack $stack;

    public function __construct(
        $env = null, $customer = null, $username = null, $password = null, $tokenPath = null, $dev = null
    ) {
        if ($env === 'production') {
            // Production
            $this->baseUrl = "https://webservices$customer.aspos.nl";
        } else {
            // Acceptance
            $this->baseUrl = "https://acceptatiewebservices$customer.aspos.nl";
        }

        $this->username = $username;
        $this->password = $password;

        if ($tokenPath === null) {
            if ($env === 'production') {
                $this->tokenPath = __DIR__ . '/../oauth-token-production.json';
            }
            else {
                $this->tokenPath = __DIR__ . '/../oauth-token.json';
            }
        } else {
            $this->tokenPath = $tokenPath;
        }

        $this->initStack();
        if ($dev === null && dev() || $dev) {
            $this->addDevMiddleware();
        }
    }

    public function initStack()
    {
        // Authorization client - this is used to request OAuth access tokens
        $authClient = new Client([
            // URL for access_token request
            'base_uri' => $this->baseUrl . '/connect/token',
        ]);
        $authConfig = [
            'client_id' => $this->username,
            'client_secret' => $this->password,
//            'scope' => '', // optional
//            'state' => time(), // optional
        ];
        $tokenPersistence = new FileTokenPersistence($this->tokenPath);
        $grantType = new ClientCredentials($authClient, $authConfig);
        $oAuth = new OAuth2Middleware($grantType);
        $oAuth->setTokenPersistence($tokenPersistence);

        $this->stack = HandlerStack::create();
        $this->stack->push($oAuth);
    }

    private function addDevMiddleware()
    {
        $this->addMiddleware(Middleware::mapRequest(function (RequestInterface $request) {
            ray()->newScreen();
            ray($request->getMethod() . ' ' . $request->getUri()->__toString());
            // ray($request->getHeaders());
            $body = $request->getBody()->getContents();
            if ($body) {
                ray(json_decode($body));
                $request->getBody()->rewind();
            }
            return $request;
        }));

        $this->addMiddleware(Middleware::mapResponse(function (ResponseInterface $response) {
            ray($response->getStatusCode());
            ray(json_decode($response->getBody()->getContents()));
            $response->getBody()->rewind();
            return $response;
        }));
    }

    public function addMiddleware(callable $handler)
    {
        $this->stack->push($handler);
    }

    public function getClient(): Client
    {
        // This is the normal Guzzle client that you use in your application
        return new Client([
            'handler' => $this->stack,
            'auth' => 'oauth',
            'base_uri' => $this->baseUrl,
        ]);
    }
}
