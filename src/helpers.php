<?php

namespace TTT\Aspos;

function dev(): bool
{
    return (getenv('COMPUTERNAME') == 'PCA');
}
